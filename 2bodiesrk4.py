import numpy as np
import scipy.integrate
import matplotlib.pyplot as plt

parameter=398600.4415
t_0=0.
t_f=3e6
N=8	
h=1.
num=int((t_f-t_0)/h)
t=np.linspace(t_0,t_f,num+1)		
y=np.zeros([N,num+1],float)					#row is a component of y, column indicates time instant
y[:,0]=np.array([0.,0.,384400.,0.,0.,0.,0.91647306922544,0.91647306922544])									#y={x1,y1,x2,y2,v1x,v1y,v2x,v2y}							

#def accel(r):
#	r1=r[:int(len(r)/2)]
#	r2=r[int(len(r)/2):]
#	a1=-parameter/(np.linalg.norm(r1-r2))**3*(r1-r2)
#	a2=-parameter/(np.linalg.norm(r2-r1))**3*(r2-r1)
#	a=np.zeros(len(r),float)
#	a[:int(len(a)/2)]=a1
#	a[int(len(a)/2):]=a2
#	return a
#def rungekutta4(yn):
#	x=yn[:int(len(yn)/2)]
#	v=yn[int(len(yn)/2):]
#	k1x=h*v
#	k1v=h*accel(x)
#	k2x=h*(v+0.5*k1v)
#	k2v=h*accel(x+0.5*k1x)
#	k3x=h*(v+0.5*k2v)
#	k3v=h*accel(x+0.5*k2x)
#	k4x=h*(v+k3v)
#	k4v=h*accel(x+k3x)
#	xnew=x+1./6.*(k1x+2*k2x+2*k3x+k4x)
#	vnew=v+1./6.*(k1v+2*k2v+2*k3v+k4v)
#	ynew=np.zeros(len(yn),float)
#	ynew[:int(len(ynew)/2)]=xnew
#	ynew[int(len(ynew)/2):]=vnew
#	return ynew
#for i in range(len(t)-1):
#	y[:,i+1]=rungekutta4(y[:,i])

def f(t,x):						#where x={x1,y1,x2,y2,v1x,v1y,v2x,v2y}
	return np.array([x[4],x[5],x[6],x[7],-parameter/(np.linalg.norm(x[0:2]-x[2:4]))**3*(x[0]-x[2]),-parameter/(np.linalg.norm(x[0:2]-x[2:4]))**3*(x[1]-x[3]),+parameter/(np.linalg.norm(x[0:2]-x[2:4]))**3*(x[0]-x[2]),+parameter/(np.linalg.norm(x[0:2]-x[2:4]))**3*(x[1]-x[3])])

def rungekutta4(funz,hn,tn,yn):
	k1=hn*funz(tn,yn)
	k2=hn*funz(tn+0.5*hn,yn+0.5*k1)
	k3=hn*funz(tn+0.5*hn,yn+0.5*k2)
	k4=hn*funz(tn+hn,yn+k3)
	ynew=yn+1./6.*(k1+2*k2+2*k3+k4)
	return ynew

for i in range(len(t)-1):
	y[:,i+1]=rungekutta4(f,h,t[i],y[:,i])

solve=scipy.integrate.solve_ivp(f,(t_0,t_f),y[:,0],method='RK45',t_eval=t,vectorized=True,max_step=100.)
y_values=solve.y

residuals=y-y_values

fig1=plt.figure()
plt.plot(y[0,:],y[1,:],'b-',lw=1,label='body 1')
plt.plot(y[2,:],y[3,:],'r-',lw=1,label='body 2')
plt.plot((y[0,:]+y[2,:])/2.,(y[1,:]+y[3,:])/2.,'g-',lw=1,label='center of mass')
plt.xlabel('x (km)')
plt.ylabel('y (km)')
plt.legend()
fig1.show()
fig2=plt.figure()
plt.plot(y_values[0,:],y_values[1,:],'b-',lw=1,label='body 1')
plt.plot(y_values[2,:],y_values[3,:],'r-',lw=1,label='body 2')
plt.plot((y_values[0,:]+y_values[2,:])/2.,(y_values[1,:]+y_values[3,:])/2.,'g-',lw=1,label='center of mass')
plt.xlabel('x (km)')
plt.ylabel('y (km)')
plt.legend()
fig2.show()
fig3=plt.figure()
plt.plot(t,residuals[0,:],'b-',lw=1,label='x1 residuals')
plt.plot(t,residuals[1,:],'b--',lw=1,label='y1 residuals')
plt.plot(t,residuals[2,:],'r-',lw=1,label='x2 residuals')
plt.plot(t,residuals[3,:],'r--',lw=1,label='y2 residuals')
plt.xlabel('time (s)')
plt.ylabel('position residual (km)')
plt.legend()
fig3.show()
fig1.tight_layout()
fig2.tight_layout()
fig3.tight_layout()
plt.show()