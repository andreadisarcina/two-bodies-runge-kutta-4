# Two Bodies Runge-Kutta 4

Numerical integrator with method Runge-Kutta 4th order for the equations of motion of a 2-body system.

A comparison with the preexisting RK45 method from scipy.integrate.solve_ivp has been done.

# Parameters of the simulation

The bodies have the mas of the Earth, and they are subject just to their mutual gravitational interaction.

The initial conditions for position and velocity are (x1,v1)=(0.,0.,0.,0.) and (x2,v2)=(384400.,0.,0.91647306922544,0.91647306922544), where positions are in km and velocities in km/s.

# Plots

The plot orbits.png shows the trajectories of the two bodies and that of the center of mass.

The plot residuals.png shows the difference in the results given by the method developed in the script and the built-in method from scipy.
